package dto;

public class Password {

	//Atributos
	public int longitud;
	public String contrasena;

	public String toString() {
		return "Password [longitud=" + longitud + ", contrasena=" + contrasena + "]";
	}

	//Constructor por defecto
	public Password() {
		this.longitud=8;
		this.contrasena="";
	}
	
	//Constructor con la longitud que nosotros le pasemos. Generara una contraseaa aleatoria con esa longitud
	public Password(int longitud) {
		this.longitud=longitud;
		this.contrasena=generarContrasena(longitud);
	}

	private String generarContrasena(int longitud) {
		String contrasena = "";
		
		for (int cont = 0; cont < longitud; cont++) {
			int randomNum = (int) ((Math.random() * (122 - 48)) + 48);
			
			contrasena += (char) randomNum;
		}
		
		return contrasena;
	}
}
